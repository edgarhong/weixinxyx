function searcha() {
	$(".prer03 li p em").live("click",
	function() {
		$(this).parents("li").css("display", "none")
	})
}
function searcher(n) {
	$(n).bind({
		keyup: function() {
			$(this).val().length > 0 ? $(this).next("span").show() : $(this).next("span").hide()
		},
		blur: function() {
			setTimeout(function() {
				$(n).next("span").hide()
			},
			500)
		},
		focus: function() {
			$(this).val().length > 0 && $(this).next("span").show()
		}
	}),
	$(n).next("span").bind("click",
	function() {
		$(this).hide(),
		$(this).prev("input").val("")
	})
}
function checkEmail(n) {
	return matchEmail(n) ? !0 : (PopUp("请输入正确的Email地址"), !1)
}
function checkTelephone(n) {
	var t = /(^18\d{9}$)|(^13\d{9}$)|(^15\d{9}$)/;
	return t.test(n) ? !0 : (PopUp("请输入正确的手机号"), !1)
}
function checkEmailAndTelephone(n) {
	var t = /^(18\d{9}|13\d{9}|15\d{9}|\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)$/;
	return t.test(n) ? !0 : (PopUp("请输入正确的手机号或Email"), !1)
}
function checkPwd(n) {
	var t = /^[a-zA-Z0-9]{8,}$/;
	return t.test(n) ? !0 : (PopUp("密码不符合要求：a~z,0~9,区分大小写,最少8位数值."), !1)
}
function matchEmail(n) {
	var t = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	return t.test(n) ? !0 : !1
}
function PopUp(n, t) {
	var r, u, e, f;
	r = $('<div id="mask" style="display:none"></div>'),
	r.appendTo("body"),
	u = $('<div class="popup w580" style="display:none"><div class="popup-hd"><a id="closex" title="关闭" class="closex closegb" href="javascript:void(0);"><span>关闭</span></a></div><h3>提示</h3><p id="alertbox-msg" class="position02"></p><div class="bgPray"><input id="alertbox-OK" class="inputBtn05 closegb" type="button" value="确定"><div class="clear"></div></div></div>'),
	u.appendTo("body"),
	e = u.find("#alertbox-msg"),
	e.html(n),
	showBox(r, u),
	f = u.find("#alertbox-OK");
	f.on("click",
	function() {
		f.off(),
		closeBox(r, u),
		typeof t == "function" && t()
	});
	r.on("click",
	function() {
		f.off(),
		r.off(),
		closeBox(r, u)
	})
}
function ConfirmBox(n, t, i) {
	var u, r, o, f, e;
	u = $('<div id="mask" style="display:none"></div>'),
	u.appendTo("body"),
	r = $('<div class="popup w580" style="display: none;"><div class="popup-hd"><a id="closex" title="关闭" class="closex closegb" href="javascript:void(0);"><span>关闭</span></a></div><h3>提示</h3><p id="alertbox-msg" class="position"></p><div class="bgPray popclear"><div class="w200"><input id="alertbox-Cancel" class="inputBtn03 closegb" type="button" value="取消"><input id="alertbox-OK" class="inputBtn04 closegb" type="button" value="确定"><div class="clear"></div></div></div></div>'),
	r.appendTo("body"),
	o = r.find("#alertbox-msg"),
	o.html(n),
	showBox(u, r),
	f = r.find("#alertbox-OK"),
	e = r.find("#alertbox-Cancel");
	f.on("click",
	function() {
		f.off(),
		e.off(),
		closeBox(u, r),
		typeof t == "function" && t()
	});
	$("#alertbox-Cancel").on("click",
	function() {
		f.off(),
		e.off(),
		closeBox(u, r),
		typeof i == "function" && i()
	});
	u.on("click",
	function() {
		f.off(),
		e.off(),
		u.off(),
		closeBox(u, r)
	})
}
function showBox(n, t) {
	n.show(),
	t.slideDown(300)
}
function closeBox(n, t) {
	t.slideUp(300,
	function() {
		n.remove(),
		t.remove()
	})
}
function nofind() {
	var n = event.srcElement;
	n.src = $("#hidDefaultImage").val(),
	n.onerror = null
}


var touchstarts = "ontouchstart" in document ? "touchstart": "mouseover",
tapse = "ontouchstart" in document ? "touchstart": "click",
touchends = "ontouchend" in document ? "touchend": "mouseout",
addclass = function(n, t) {
	$(n).bind("click",
	function() {
		$(this).addClass(t)
	})
},
slidershow = function(n, t) {
	$(n).bind("click",
	function(i) {
		var u = $(i.target);
		if (u.is("div,h3")) {
			$(this).find(t).animate({
				width: 33
			},
			500, "easeOutBounce");
			var f = $(n).index(this),
			e = $(t).eq(f).find(".listshu").html(),
			r = parseInt(e);
			r += 1,
			$(this).find(t + " .listshu").html(r)
		}
	}),
	$$(n).swipeLeft(function() {
		$(this).find(t).animate({
			width: 0
		},
		200)
	})
},
tapcolor = function(n, t) {
	t === undefined && (t = "tapclass"),
	$(n).bind(touchstarts,
	function() {
		$(this).addClass(t).siblings(n).removeClass(t)
	}),
	$(n).bind(touchends,
	function() {
		$(this).removeClass(t)
	})
},
toggleclass = function(n, t) {
	$(n).bind("click",
	function() {
		$(this).toggleClass(t)
	})
},
eventclass = function(n, t, i) {
	$(n).bind(tapse,
	function(r) {
		var u = $(r.target);
		u.is(n) && $(t).toggleClass(i)
	})
},
swval = function(n, t, i, r) {
	$(i).on("click",
	function() {
		var i = $(r).text();
		i === n ? $(r).text(t) : $(r).text(n)
	})
},
mulitab = function(n, t, i) {
	$(t).click(function() {
		var r = $(t).index(this);
		$(t).removeClass(i),
		$(this).addClass(i),
		$(n).hide(),
		$(n).eq(r).show()
	})
},
fBansize = function(n, t) {
	var i = $("body").width(),
	r = i * 7 / 16;
	$(n).height(r),
	$(t).width(i),
	$(t).height(r)
},
flyfun = function(n, t, i) {
	$(n).bind("click",
	function(n) {
		var u = $(n.target);
		if (u.is("div,h3")) {
			$(t).find(".fly").remove(),
			$(this).parent().append('<div class="fly" style="position:absolute;top:40%;left:40%;width:18px;height:18px;border-radius:25px;background:#ea5413; opacity:0.5"></div>');
			var f = $(".fly").offset().top,
			e = $(i).offset().top + 50,
			r = e - f;
			r += "px",
			$(".fly").animate({
				width: "10px",
				height: "10px",
				top: r,
				left: "0px",
				opacity: "0.5"
			},
			function() {
				$(this).parent().find(".fly").remove()
			})
		}
	})
},
counter = function(n, t) {
	$(n).on("click",
	function() {
		var u = '<div class="qipao">0</div>',
		n = $(this).parent().index(),
		f = $(t + " ul li").eq(n).find(".qipao").length,
		e = Boolean(f),
		r,
		i;
		e || $(t + " ul li").eq(n).append(u),
		r = $(t + " ul li").eq(n).find(".qipao").text(),
		i = parseInt(r),
		i += 1,
		$(t + " ul li").eq(n).find(".qipao").text(i),
		$(".qipao").css({
			position: "absolute",
			top: "8px",
			right: "5px",
			width: "16px",
			height: "16px",
			"line-height": "13px",
			"text-align": "center",
			color: "#fff",
			background: "url(images/icos/qipao.png) 0 0 no-repeat",
			backgroundSize: "16px 16px"
		})
	})
},
conHei = function() {
	for (var i = $("html").height(), t = i - 84, n = 0; n < arguments.length; n++) $("" + arguments[n] + "").height(t)
},
conHei2 = function() {
	for (var i = $("html").height(), t = i - 204, n = 0; n < arguments.length; n++) $("" + arguments[n] + "").height(t)
},
toolbar = function(n, t) {
	var i = parseInt($(n).offset().top),
	r = parseInt($(n).offset().left);
	$(window).scroll(function() {
		$(window).scrollTop() > i ? $(n).attr("style", "position:fixed ;left:" + r + "px; top:" + t + "; z-index:99999") : $(n).removeAttr("style")
	})
},
heightUl = function(n) {
	var t = $(window).height() - 54;
	$(n).css({
		height: t + "px"
	})
};
searcha(),
$(function() {
	tapcolor("header div"),
	tapcolor("footer nav ul li")
}),
sliderbottom = function(n, t) {
	$(n).animate({
		bottom: 0
	},
	500),
	$(n).find(t).bind(tapse,
	function() {
		$(n).animate({
			bottom: "-43px"
		},
		500)
	})
},
vostai = function(n, t) {
	$(n).bind(touchstarts,
	function() {
		$(this).addClass(t)
	}),
	$(n).bind(touchends,
	function() {
		$(this).removeClass(t)
	})
},
$(function() {
	tapcolor("header div"),
	tapcolor("footer nav ul li")
}),
$(function() {
	searcher(".searchcloae")
}),
$("#onloading").bind("ajaxSend",
function() {
	$("#loading").show()
}).bind("ajaxComplete",
function() {
	$("#loading").hide()
}),
$(document).ready(function() {
	$(".return").click(function(n) {
		$(this).attr("id") != "noreturn" && (location.href = $("#hidReturnUrl").val()),
		n.stopPropagation()
	})
}),


/**
 * 返回顶部
 */
$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
$('.go_top').click(function() {
	window.scrollTo(0, 0);
});
$(window).bind('scroll', function() {
	var scroll_top = parseInt($('body')[0].scrollTop);
	if (scroll_top >= 450) {
		$('.go_top').css('display', 'block');
	} else {
		$('.go_top').css('display', 'none');
	}
});

var scroll_top = parseInt($('body')[0].scrollTop);
if (scroll_top >= 450) {
	$('.go_top').css('display', 'block');
} else {
	$('.go_top').css('display', 'none');
}
