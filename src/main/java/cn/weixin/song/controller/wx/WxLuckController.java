package cn.weixin.song.controller.wx;

import java.util.List;

import cn.weixin.song.contant.R;
import cn.weixin.song.interceptor.WeixinAccessInterceptor;
import cn.weixin.song.model.Activity;
import cn.weixin.song.model.ActivityAward;
import cn.weixin.song.model.ActivityShareRecord;
import cn.weixin.song.model.ActivityUserPlay;
import cn.weixin.song.model.App;
import cn.weixin.song.model.User;
import cn.weixin.song.util.WeixinUtils;

import com.github.sd4324530.fastweixin.api.response.GetSignatureResponse;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.view.InvokeResult;
import com.jfinal.aop.Before;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 幸运转盘
 * @author eason
 */
public class WxLuckController extends JCBaseController {
    
	@Before(WeixinAccessInterceptor.class)
	public void index() {
		Integer aid=this.getParaToInt("aid");
		Integer addplaynum = this.getParaToInt("addplaynum");
		if(aid!=null){
			String openid=this.getPara("openid");
			if(StrKit.isBlank(openid)){
				openid=this.getCookie("openid");
			}
			this.setAttr("openid",openid);
			this.setCookie("openid", openid, 60*60*24*30);
			
			Activity activity=Activity.dao.findById(aid);
			
			if(addplaynum!=null&&addplaynum>0){
				User user=User.dao.getUser(activity.getAppId(), openid);
				ActivityUserPlay activityUserPlay=ActivityUserPlay.dao.getActivityUserPlay(user.getId(), activity.getId());
				if(activityUserPlay!=null){
					activityUserPlay.setLeftPlayCount(activityUserPlay.getLeftPlayCount()+1);
					activityUserPlay.update();//成功购票增加抽奖次数功能
				}
			}
			
			if(activity!=null){
		        App app=App.dao.findById(activity.getAppId());
		        this.setAttr("wxAppId",app.getWxAppId());
		        this.setAttr("app",app); //=http://119.29.144.28:8082/wx/luck?aid=1&openid=oT4TvwTQBg_Y4KJNkfYfoY8fM9wk&from=singlemessage
		        String surl = this.getThisUrl() ; 
		        String fxurl = "" ; 
		        if(surl!=null&&!surl.equals("")){
		        	String []s = surl.split("8082");
		        	fxurl = R.Url.url+s[1];
		        }
				GetSignatureResponse  getSignatureResponse =WeixinUtils.getJsAPI(app.getWid()).getSignature(fxurl);
				this.setAttr("appData", getSignatureResponse); 
				List<ActivityAward> awardlist=ActivityAward.dao.getActivityAwardList(aid);
				String[] awardNames=new String[awardlist.size()];
				String[] colors=new String[awardlist.size()];
				for(int i=0;i<awardlist.size();i++){
					ActivityAward item =awardlist.get(i);
					awardNames[i]=item.getName();
					colors[i]=item.getBgColor();
				}
				this.setAttr("awardNames", JsonKit.toJson(awardNames));
				this.setAttr("colors", JsonKit.toJson(colors));
				this.setAttr("activity",activity);
				this.setAttr("openid", openid);
				this.render("luck_index.jsp");
				return;
			}
		}
		this.renderError(404);
    }
	
	/**
	 * 抽奖接口
	 */
	@Before(Tx.class)
	public void lottery(){ 
		String openid=this.getPara("openid");
		Integer aid=this.getParaToInt("aid");
		InvokeResult result=Activity.dao.lottery(openid,aid);
		this.renderJson(result);
	}

	/**
	 * 提交用户信息
	 * @author eason
	 */
	@Before(Tx.class)
	public void submitInfo(){
		String openid=this.getPara("openid"); 
		Integer appId=this.getParaToInt("appId"); 
		String mobilephone=this.getPara("mobilephone");
		InvokeResult result=User.dao.updateUserInfo(appId,openid,mobilephone);
		this.renderJson(result);
	}
	
	/**
	 * 分享记录
	 */
	@Before(Tx.class)
	public void share(){ 
		String openid=this.getPara("openid");
		Integer type=this.getParaToInt("type");
		Integer aid=this.getParaToInt("aid");
		System.out.println("1111="+openid+"===="+type+"====="+aid);
		if(StrKit.isBlank(openid)||type==null||aid==null){
			this.renderJson( InvokeResult.failure(""));
			return;
		}
		Activity activity=Activity.dao.findById(aid);
		System.out.println("222="+activity);
		InvokeResult result=ActivityShareRecord.dao.addActivityShareRecord(activity,openid,type);
		this.renderJson(result);
	}
}
