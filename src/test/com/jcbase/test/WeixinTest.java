package com.jcbase.test;

import java.util.List;

import org.junit.Test;

import cn.weixin.song.model.App;
import cn.weixin.song.model.AppMenu;

import com.google.common.collect.Lists;
import com.jcbase.conf.JcConfig;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.RandomUtils;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
/**
 * 应用启动入口
 * @author eason
 *
 */
public class WeixinTest {
	@Test
	public void testCreateAppMenuData(){
		PropKit.use("base_config.txt");
		DruidPlugin dp = JcConfig.createDruidPlugin();
		ActiveRecordPlugin arp1 = new ActiveRecordPlugin(dp);
		arp1.addMapping("app", App.class);
		arp1.addMapping("app_menu", AppMenu.class);
		// 与web环境唯一的不同是要手动调用一次相关插件的start()方法
		dp.start();
		arp1.start();
		App app=App.dao.getApp("ysrlesgif5wvwb8rgsxpff7i28ge0pos");
		//删除旧的菜单数据
		AppMenu.dao.delete(CommonUtils.getConditions(new Condition("app_id",Operators.EQ,app.getId())));
		//创建新菜单数据
		AppMenu topMenu1=new AppMenu();
		topMenu1.setName("首页");
		topMenu1.setType("VIEW");//页面跳转类型
		topMenu1.setUrl("http://www.hntuji.com/");
		topMenu1.setMenuKey(RandomUtils.getRandomString(15).toLowerCase());
		topMenu1.setSeq(1);//排序号
		topMenu1.setAppId(app.getId());
		topMenu1.save();
		
		AppMenu topMenu2=new AppMenu();
		topMenu2.setName("转盘活动");
		topMenu2.setType("CLICK");//点击事件类型
		topMenu2.setSeq(2);
		topMenu2.setAppId(app.getId());
		topMenu2.save();
		
		AppMenu topMenu2Sub1=new AppMenu();
		topMenu2Sub1.setName("玩转转盘");
		topMenu2Sub1.setType("CLICK");
		topMenu2Sub1.setMenuKey(RandomUtils.getRandomString(15).toLowerCase());
		topMenu2Sub1.setSeq(1);
		topMenu2Sub1.setRefType(1);//判断进公众号转盘菜单
		topMenu2Sub1.setRefId(1);//相关的活动ID 
		topMenu2Sub1.setPid(topMenu2.getId());
		topMenu2Sub1.setAppId(app.getId());
		topMenu2Sub1.save();
		
		AppMenu topMenu2Sub2=new AppMenu();
		topMenu2Sub2.setName("我的优惠券");
		topMenu2Sub2.setType("CLICK");
		topMenu2Sub2.setMenuKey(RandomUtils.getRandomString(15).toLowerCase());
		topMenu2Sub2.setSeq(2);
		topMenu2Sub2.setRefType(2);//判断进公众号转盘菜单
		topMenu2Sub2.setRefId(1);//相关的活动ID 
		topMenu2Sub2.setPid(topMenu2.getId());
		topMenu2Sub2.setAppId(app.getId());
		topMenu2Sub2.save();
		
		AppMenu topMenu3=new AppMenu();
		topMenu3.setName("联系我们");
		topMenu3.setType("CLICK");
		topMenu3.setMenuKey(RandomUtils.getRandomString(15).toLowerCase());
		topMenu3.setContent("活动咨询：Jack(手机号13333333333)\n商务合作：Eason(手机号13233333333)");
		topMenu3.setSeq(3);
		topMenu3.setAppId(app.getId());
		topMenu3.save();
	}
	
	@Test
	public void testRefreshAppMenu(){
		PropKit.use("base_config.txt");
		DruidPlugin dp = JcConfig.createDruidPlugin();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
		arp.addMapping("app", App.class);
		arp.addMapping("app_menu", AppMenu.class);
		// 与web环境唯一的不同是要手动调用一次相关插件的start()方法
		dp.start();
		arp.start();
		AppMenu.dao.refreshAppMenu("ysrlesgif5wvwb8rgsxpff7i28ge0pos");
	}
}
